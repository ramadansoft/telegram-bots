<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Bot;
use Illuminate\Http\Request;
use App\Models\Delivery as DeliveryModel;
use App\Models\DeliveryType;
use App\Models\ShurarbPost;
use App\Jobs\TelegramDelivery;

class Shurarb extends Controller {

  function newPost(Request $request) {
    $sender = app('ShuraRBBot'); /* @var $sender \App\Services\Bots\ShuraRB */
    $sender->request = null;
    $sender->bot = Bot::where('key','136405440:AAF7uE0z08TXmRSAYnD9XJo317GNuG76tTc')->first();

    // Необходимо очищать от арабских символов текст, иначе Телеграм не станет принимать сообщение
    //$clean_arabic = function($text) { return preg_replace('/[أ-ي]/ui', '', $text); };

    $post_id    = $request->input('id');
    $post_title = $request->input('title');
    //$post_text  = mb_strimwidth($clean_arabic(strip_tags(trim($request->input('text')))), 0, 700, "...");
    $post_uri   = $request->input('uri');

    $post_in_db = ShurarbPost::findOrNew($post_id);
    if ($post_in_db->exists) {
      return 'post already exists in db';
    }

    $post_in_db->id = $post_id;
    $post_in_db->save();

    //app('log')->info("New message about new post from Shurarb.ru. Title: {$post_title}. Text: {$post_text}. Uri: {$post_uri}");

    $delivery = new DeliveryModel;
    $delivery->status_id = DELIVERY_STATUS_WAITING;
    $delivery->type_id   = DeliveryType::first()->id;
    $delivery->users_id  = User::where('nick','BelieverUfa')->first()->id;
    //$delivery->text      = "{$post_title}.\n\n{$post_text}\n\n{$post_uri}";
    $delivery->text      = "{$post_title}.\n\n{$post_uri}";
    $delivery->save();

    $job = new TelegramDelivery($delivery);
    app('Illuminate\Contracts\Bus\Dispatcher')->dispatch($job);

    /*
    foreach(User::where('admin',true)->get() as $user) {
        $sender->sendAnswer($sender->createReply([
          'text' => "{$post_title}.\n\n{$post_text}\n\n{$post_uri}"
        ]), $user->id);
    }
    */
  }

}

