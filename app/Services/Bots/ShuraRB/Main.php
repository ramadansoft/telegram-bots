<?php

namespace App\Services\Bots\ShuraRB;

use App\Services\BotsService;

/**
 * Основные команды
 */
class Main extends BotsService {

  function commandStart() {
    return [
      'text' => "Ас-саляму алейкум уа рахматуллахи уа баракятух, {$this->user->name()}!\n\nВы подключились к рассылкам Шуры мусульман Башкортостана. С этого момента вы будете получать сообщения с данного бота.",
      'commands' => [
        'help' => 'Помощь по работе с ботом'
      ],
    ];
  }

  function commandHelp() {
    $answer['commands'] = [
      //'select_types' => 'Выбрать типы рассылок, на которые можно подписаться.',
      'delivery_request' => 'Предложить разослать какое-то сообщение.',
      'namaz' => 'Показать время начала намазов на сегодня.',
    ];
    if ($this->checkForAdmin()) {
      $answer['commands']['new_delivery'] = 'Создать новую рассылку';
    }

    if ($this->checkForSuperAdmin()) {
      $answer['commands']['manage_admins'] = 'Управлять администраторами бота';
    }
    return $answer;
  }

  function commandCancel() {
    if ($this->user->state) {
      $this->user->state = null;
      $this->user->save();
      return [
        'text' => "Текущая операция отменена",
        'commands' => [
          'help' => 'Помощь в работе с ботом',
        ]
      ];
    } else {
      return "У вас нет активных операций, поэтому и отменять вам нечего.";
    }
  }

}
