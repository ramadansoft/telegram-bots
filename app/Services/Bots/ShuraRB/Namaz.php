<?php

namespace App\Services\Bots\ShuraRB;

use App\Services\BotsService;
use DateInterval;
use DateTime;
use DiDom\Document;
use Illuminate\Support\Debug\Dumper;

/**
 * Время намаза
 */
class Namaz extends BotsService {

  function commandNamaz() {
    
    $times = $this->getFromIslamicFinder2016();
    //$times = $this->getFromNamazTimeRu();
    
    if (empty($times)) {
      return 'К сожалению, по какой-то причине я не смог получить время намаза на сегодня :(';
    }
    
    $month        = date('n');
    $current_day  = date('j');
    $current_year = date('Y');
    $months_rus = [
      1 => 'января',
      2 => 'февраля',
      3 => 'марта',
      4 => 'апреля',
      5 => 'мая',
      6 => 'июня',
      7 => 'июля',
      8 => 'августа',
      9 => 'сентября',
      10 => 'октября',
      11 => 'ноября',
      12 => 'декабря'
    ];

    $text = "Время намаза на {$current_day} {$months_rus[$month]} {$current_year}:\n"
      ."Фаджр: {$times[0]}\n"
      . "Восход: {$times[1]}\n"
      . "Зухр: {$times[2]}\n"
      . "Аср: {$times[3]}\n"
      . "Магриб: {$times[5]}\n"
      . "Иша: {$times[6]}"
    ;
    
    return [
      'text' => $text,
      'commands' => [
        'help' => 'Прочие команды бота',
      ]
    ];
  }
  
  function getFromNamazTimeRu() {
    $document = new Document('http://www.vremyanamaza.ru/%D0%A3%D1%84%D0%B0/%D0%92%D1%80%D0%B5%D0%BC%D1%8F_%D0%BD%D0%B0%D0%BC%D0%B0%D0%B7%D0%B0-%D0%A3%D1%84%D0%B0-31472', true);
    $tableElements = $document->find('.timesToday tr:nth-child(3) td');
    $times = array_map(function($el) {
      return preg_replace("/([^0-9:()])/","",$el->text());
    }, $tableElements);
    // app('log')->info(json_encode($times));
    return $times;
  }
  
  // Время намаза "Аср" рассчитано согласно правовой школы имама Аш-Шафии.
  // Данное расписание сформировано при помощи сервиса http://islamicfinder.org, с учетом широты местоположения г. Уфы,
  // когда летом полная ночь не наступает.
  function getFromIslamicFinder() {
    $xml_data = app('cache')->remember('islamic-finder-times', 60, function() {
      try {
        //return simplexml_load_file('http://www.islamicfinder.org/prayer_service.php?country=russia&city=ufa&state=08&zipcode=&latitude=54.7384&longitude=55.9831&timezone=5&HanfiShafi=1&pmethod=5&fajrTwilight1=10&fajrTwilight2=10&ishaTwilight=0&ishaInterval=0&dhuhrInterval=1&maghribInterval=1&dayLight=0&simpleFormat=xml&monthly=1&month=');
        return (array) simplexml_load_file('http://www.islamicfinder.org/prayer_service.php?country=russia&city=ufa&state=08&zipcode=&latitude=54.7384&longitude=55.9831&timezone=5&HanfiShafi=1&pmethod=7&fajrTwilight1=0&fajrTwilight2=15.2&ishaTwilight=0&ishaInterval=90&dhuhrInterval=1&maghribInterval=1&dayLight=0&page_background=FFFFFF&table_background=FFFFFF&table_lines=e5e5e5&text_color=777777&link_color=336699&prayerFajr=Fajr&prayerSunrise=Sunrise&prayerDhuhr=Dhuhr&prayerAsr=Asr&prayerMaghrib=Maghrib&prayerIsha=Isha&lang=&simpleFormat=xml');
      } catch (\ErrorException $error) {
          return null;
      }
    });
    
    if (is_null($xml_data)) return null;
    
    return [
      0 => DateTime::createFromFormat('g:i',$xml_data['fajr']   )->format('G:i'),
      1 => DateTime::createFromFormat('g:i',$xml_data['sunrise'])->format('G:i'),
      2 => DateTime::createFromFormat('g:i',$xml_data['dhuhr']  )->add(new DateInterval('PT12H'))->format('G:i'),
      3 => DateTime::createFromFormat('g:i',$xml_data['asr']    )->add(new DateInterval('PT12H'))->format('G:i'),
      4 => 'аср по ханафи не расчитывается',
      5 => DateTime::createFromFormat('g:i',$xml_data['maghrib'])->add(new DateInterval('PT12H'))->format('G:i'),
      6 => DateTime::createFromFormat('g:i',$xml_data['isha']   )->add(new DateInterval('PT12H'))->format('G:i'),
    ];
  }

  function getFromIslamicFinder2016() {
    $data = app('cache')->remember('islamic-finder-times', 60, function() {
      try {
        $document = new Document('http://islamicfinder.org/prayer-widget/479561/shafi/north-america?0', true);
        $els = $document->find('table td');
        
        return array_map(function($el) {
          return preg_replace("/([^0-9:()])/","",trim($el->text()));
        }, $els);
      } catch (\ErrorException $error) {
          return null;
      }
    });
    
    if (is_null($data)) return null;
    
    return [
      0 => DateTime::createFromFormat('g:i', $data[5]  )->format('G:i'),
      1 => DateTime::createFromFormat('g:i', $data[7]  )->format('G:i'),
      2 => DateTime::createFromFormat('g:i', $data[9]  )->add(new DateInterval('PT12H'))->format('G:i'),
      3 => DateTime::createFromFormat('g:i', $data[11] )->add(new DateInterval('PT12H'))->format('G:i'),
      4 => 'аср по ханафи не расчитывается',
      5 => DateTime::createFromFormat('g:i', $data[13] )->add(new DateInterval('PT12H'))->format('G:i'),
      6 => DateTime::createFromFormat('g:i', $data[15] )->add(new DateInterval('PT12H'))->format('G:i'),
    ];
  }

}