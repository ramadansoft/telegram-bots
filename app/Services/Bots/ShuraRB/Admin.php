<?php

namespace App\Services\Bots\ShuraRB;

use App\Models\User;
use App\Services\BotsService;

/**
 * Админка
 */
class Admin extends BotsService {

  /**
   * Управление админами
   */
  function commandManageAdmins() {
    if (! $this->checkForSuperAdmin()) { return; }

    $text = "Текущие администраторы бота:\n";
    $admins = User::where('admin',true)->get();
    foreach($admins as $admin) {
      $text .= "- {$admin->name()}\n";
    }
    return [
      'text' => $text,
      'commands' => [
        'delete_admin'  => 'Удалить кого-то из админов',
        'add_admin'     => 'Добавить нового админа',
        'help'          => 'Основные команды',
      ]
    ];
  }

  /**
   * Добавление нового админа
   */
  function commandAddAdmin() {
    if (! $this->checkForSuperAdmin()) { return; }
    $this->user->state = 'add_admin';
    $this->user->save();
    return [
      'text' => "Укажите ник пользователя, которого вы желаете сделать администратором. Имейте ввиду, что этот пользователь уже должен быть подписан на рассылку данного бота. Вы можете скопировать его ник из его профиля Telegram'а.",
      'commands' => [
        'manage_admins'  => 'Управление админами',
        'cancel'         => 'Отменить операцию',
      ]
    ];
  }

  function commandAddAdminMessages() {
    if (! $this->checkForSuperAdmin()) { return; }

    if (property_exists($this->request->message, 'text')) {
      $new_admin = User::where('nick',str_replace('@','',$this->request->message->text))->first();
      if ($new_admin) {
        $new_admin->admin = true;
        $new_admin->save();
        return [
          'text' => "Вы добавили пользователя {$new_admin->name()} в список администраторов вашего бота.",
        ];
      } else {
        return [
          'text' => "Пользователь с указанным вами ником не был найден среди подписчиков бота.",
          'commands' => [
            'manage_admins'  => 'Управление админами',
            'cancel'         => 'Отменить операцию',
          ]
        ];
      }
    }
  }

  /**
   * Удаление одного из существующих админов
   */
  function commandDeleteAdmin() {
    if (! $this->checkForSuperAdmin()) { return; }
    $this->user->state = 'delete_admin';
    $this->user->save();
    return [
      'text' => "Укажите ник пользователя, которого вы желаете удалить из администраторов. Вы можете скопировать его ник из его профиля Telegram'а.",
      'commands' => [
        'manage_admins'  => 'Управление админами',
        'cancel'         => 'Отменить операцию',
      ]
    ];
  }

  function commandDeleteAdminMessages() {
    if (! $this->checkForSuperAdmin()) { return; }

    if (property_exists($this->request->message, 'text')) {
      $old_admin = User::where('nick',str_replace('@','',$this->request->message->text))->first();
      if ($old_admin) {
        $old_admin->admin = false;
        $old_admin->save();
        return [
          'text' => "Вы удалили пользователя {$old_admin->name()} из списка администраторов вашего бота.",
        ];
      } else {
        return [
          'text' => "Пользователь с указанным вами ником не был найден среди подписчиков бота.",
          'commands' => [
            'manage_admins'  => 'Управление админами',
            'cancel'         => 'Отменить операцию',
          ]
        ];
      }
    }
  }

}
