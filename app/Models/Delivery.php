<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Delivery extends Model {

	protected $table = 'deliveries';

  protected $fillable = ['text','file','type_id','status_id','users_id'];

  function type() {
    return $this->belongsTo('App\Models\DeliveryType','type_id');
  }

  function status() {
    return $this->belongsTo('App\Models\DeliveryStatus','status_id');
  }

  function user() {
    return $this->belongsTo('App\Models\User','users_id');
  }

}
